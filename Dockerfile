FROM golang:1.18-alpine as builder

WORKDIR /usr/src/app
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN go build -buildvcs=false -v -o /usr/local/bin/gold_tracker ./cmd/gold_tracker

FROM alpine

COPY --from=builder /usr/local/bin/gold_tracker /usr/bin/gold_tracker
EXPOSE 5000
CMD /usr/bin/gold_tracker
