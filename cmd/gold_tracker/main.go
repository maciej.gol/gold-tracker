package main

import (
	"context"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gold_tracker/pkg/mennica"
	"gold_tracker/pkg/mennica_kapitalowa"
	"gold_tracker/pkg/mennica_skarbowa"
	"net/http"
	"sync"
	"time"
)

var goldPrice = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: "gold_price",
}, []string{"source", "name", "type", "weight_grams"})

func init() {
	prometheus.MustRegister(goldPrice)
}

func runMennica(ctx context.Context) {
	scraper := mennica.Scraper{}
	ticker := time.NewTicker(30 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			for _, entry := range scraper.Scrape() {
				goldPrice.WithLabelValues("mennica.com.pl", entry.Name, entry.Type, entry.WeightGrams).Set(entry.Price)
			}
		}
	}
}

func runSkarbowa(ctx context.Context) {
	scraper := mennica_skarbowa.Scraper{}
	ticker := time.NewTicker(30 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			for _, entry := range scraper.Scrape() {
				goldPrice.WithLabelValues("mennicaskarbowa.pl", entry.Name, entry.Type, entry.WeightGrams).Set(entry.Price)
			}
		}
	}
}

func runKapitalowa(ctx context.Context) {
	scraper := mennica_kapitalowa.Scraper{}
	ticker := time.NewTicker(30 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			for _, entry := range scraper.Scrape() {
				goldPrice.WithLabelValues("mennicakapitalowa.pl", entry.Name, entry.Type, entry.WeightGrams).Set(entry.Price)
			}
		}
	}
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}

	go func() {
		wg.Add(1)
		runMennica(ctx)
		wg.Done()
	}()

	go func() {
		wg.Add(1)
		runSkarbowa(ctx)
		wg.Done()
	}()

	go func() {
		wg.Add(1)
		runKapitalowa(ctx)
		wg.Done()
	}()

	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":5000", nil)
	cancel()
	wg.Wait()
}
