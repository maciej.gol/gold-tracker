package mennica

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
)

type Entry struct {
	Name        string
	Price       float64
	Type        string
	WeightGrams string
}

type EntryTemplate struct {
	Name        string
	WeightGrams string
	URL         string
	Type        string
}

type Scraper struct {
	client http.Client
}

var urlsToScrape = []EntryTemplate{
	{"Britannia", "31.1", "https://inwestycje.mennica.com.pl/produkty-inwestycyjne-zloto-monety-bulionowe-britannia-1-oz-zlota-moneta-bulionowa/", "coin"},
	{"Filharmonicy", "31.1", "https://inwestycje.mennica.com.pl/filharmonicy-wiedenscy-1-t-oz/", "coin"},
	{"Kangur", "31.1", "https://inwestycje.mennica.com.pl/kangaroo-1-oz-a-100/", "coin"},
	{"Bizon", "31.1", "https://inwestycje.mennica.com.pl/amerykanski-bizon-1-t-oz/", "coin"},
	{"Amerykanski Orzel", "31.1", "https://inwestycje.mennica.com.pl/amerykanski-orzel-1-t-oz/", "coin"},
	{"Sztabka Mennicy", "31.1", "https://inwestycje.mennica.com.pl/produkty-inwestycyjne/zloto/sztabki-mennicy-polskiej/1oz-2/", "bar"},
	{"Sztabka Mennicy", "20", "https://inwestycje.mennica.com.pl/produkty-inwestycyjne/zloto/sztabki-mennicy-polskiej/tak/", "bar"},
	{"Sztabka Valcambi", "20", "https://inwestycje.mennica.com.pl/produkty-inwestycyjne/zloto/sztabki-mennicy-polskiej/sztabka-zlota-valcambi-20-g/", "bar"},
}

func (s Scraper) Scrape() []*Entry {
	result := []*Entry{}
	priceRegexp := regexp.MustCompile(`itemprop="price" content="(\d+)"`)
	for _, template := range urlsToScrape {
		fmt.Println("Fetching", template.URL)
		resp, err := s.client.Get(template.URL)
		entry := &Entry{
			Name:        template.Name,
			Price:       0.0,
			WeightGrams: template.WeightGrams,
			Type:        template.Type,
		}
		result = append(result, entry)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		defer resp.Body.Close()
		rawBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		matches := priceRegexp.FindStringSubmatch(string(rawBody))
		if len(matches) != 2 {
			fmt.Println("Invalid matches:", len(matches))
			continue
		}
		price, err := strconv.ParseFloat(matches[1], 32)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		entry.Price = price
		fmt.Println(entry.Name, entry.Price)
	}
	return result
}
