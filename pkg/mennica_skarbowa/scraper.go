package mennica_skarbowa

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
)

type Entry struct {
	Name        string
	Price       float64
	WeightGrams string
	Type        string
}

type EntryTemplate struct {
	Name        string
	WeightGrams string
	URL         string
	Type        string
}

type Scraper struct {
	client http.Client
}

var urlsToScrape = []EntryTemplate{
	{"Britannia", "31.1", "https://www.mennicaskarbowa.pl/product-pol-15-Moneta-Britannia-1-uncja-zlota.html", "coin"},
	{"Filharmonicy", "31.1", "https://www.mennicaskarbowa.pl/product-pol-16-Moneta-Wiedenscy-Filharmonicy-1-uncja-zlota.html", "coin"},
	{"Kangur", "31.1", "https://www.mennicaskarbowa.pl/product-pol-4-Moneta-Australijski-Kangur-1-uncja-zlota.html", "coin"},
	{"Bizon", "31.1", "https://www.mennicaskarbowa.pl/product-pol-5-Amerykanski-Bizon-1-uncja-zlota.html", "coin"},
	{"Lisc Klonowy", "31.1", "https://www.mennicaskarbowa.pl/product-pol-14-Moneta-Kanadyjski-Lisc-Klonowy-1-uncja-zlota.html", "coin"},
	{"Rok Tygrysa 24h", "31.1", "https://www.mennicaskarbowa.pl/product-pol-8276-Moneta-Rok-Tygrysa-1-uncja-zlota-2022-wysylka-24h.html", "coin"},
	{"Biały Chart 24h", "31.1", "https://www.mennicaskarbowa.pl/product-pol-7438-Bestie-Krolowej-Bialy-Chart-z-Richmond-1-uncja-zlota-wysylka-24h.html", "coin"},
	{"Sztabka", "31.1", "https://www.mennicaskarbowa.pl/product-pol-11-1-uncja-Sztabka-zlota.html", "bar"},
	{"Sztabka", "20", "https://www.mennicaskarbowa.pl/product-pol-10-20-g-Sztabka-zlota.html", "bar"},
}

func (s Scraper) Scrape() []*Entry {
	result := []*Entry{}
	priceRegexp := regexp.MustCompile(`"price_net":"([\d.]+)"`)
	for _, template := range urlsToScrape {
		fmt.Println("Fetching", template.URL)
		resp, err := s.client.Get(template.URL)
		entry := &Entry{
			Name:        template.Name,
			Price:       0.0,
			WeightGrams: template.WeightGrams,
			Type:        template.Type,
		}
		result = append(result, entry)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		defer resp.Body.Close()
		rawBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		matches := priceRegexp.FindStringSubmatch(string(rawBody))
		if len(matches) != 2 {
			fmt.Println("Invalid matches:", len(matches))
			continue
		}
		price, err := strconv.ParseFloat(matches[1], 32)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		entry.Price = price
		fmt.Println(entry.Name, entry.Price)
	}
	return result
}
