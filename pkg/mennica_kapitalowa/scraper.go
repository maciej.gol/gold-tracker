package mennica_kapitalowa

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
)

type Entry struct {
	Name        string
	Price       float64
	WeightGrams string
	Type        string
}

type EntryTemplate struct {
	Name        string
	WeightGrams string
	URL         string
	Type        string
}

type Scraper struct {
	client http.Client
}

var urlsToScrape = []EntryTemplate{
	{"Smok 2022 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-7204-zlota-moneta-z-Serii-Chinskie-Mity-i-Legendy-Smok-2022-1oz-24h.html", "coin"},
	{"Lion of England 2022 20d", "31.1", "https://mennicakapitalowa.pl/product-pol-7156-moneta-zlota-1oz-The-Royal-Tudor-Beasts-Lion-of-England-2022.html", "coin"},
	{"Somalijski Słoń 2022 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-7001-moneta-zlota-Somalijski-Slon-2022-1oz-24h.html", "coin"},
	{"Bestie Królowej 2021 15d", "31.1", "https://mennicakapitalowa.pl/product-pol-6541-moneta-zlota-1oz-Bestie-Krolowej-2021.html", "coin"},
	{"Podwójny Pixiu 2021 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-6539-moneta-1oz-zlota-Australia-Podwojny-Pixiu-2021-24h.html", "coin"},
	{"Bestie Królowej 2021 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-6533-moneta-zlota-1oz-Bestie-Krolowej-2021-24h.html", "coin"},
	{"Emu 2021 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-6406-Moneta-zlota-Australijski-Emu-2021-1-uncja-24h.html", "coin"},
	{"Krugerrand 2022 20d", "31.1", "https://mennicakapitalowa.pl/product-pol-434-moneta-zlota-Krugerrand-1oz-2022-najtaniej.html", "coin"},
	{"Krugerrand 2022 10d", "31.1", "https://mennicakapitalowa.pl/product-pol-7-moneta-zlota-Krugerrand-1oz-2022.html", "coin"},
	{"Bizon 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-229-monety-zlote-Amerykanski-Bizon-1oz-24h.html", "coin"},
	{"Bizon 10d", "31.1", "https://mennicakapitalowa.pl/product-pol-55-moneta-zlota-Amerykanski-Bizon-1oz.html", "coin"},
	{"Amerykański Orzeł 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-228-monety-zlote-Amerykanski-Orzel-1oz-24h.html", "coin"},
	{"Amerykański Orzeł 10d", "31.1", "https://mennicakapitalowa.pl/product-pol-56-moneta-zlota-Amerykanski-Orzel-1oz.html", "coin"},
	{"Wiedeński Filharmonik 2022 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-208-monety-zlote-Wiedenski-Filharmonik-1oz-2022-24h.html", "coin"},
	{"Wiedeński Filharmonik 2022 20d", "31.1", "https://mennicakapitalowa.pl/product-pol-148-moneta-zlota-Wiedenski-Filharmonik-1oz-2022-najtaniej.html", "coin"},
	{"Wiedeński Filharmonik 2022 10d", "31.1", "https://mennicakapitalowa.pl/product-pol-51-monety-zlote-Wiedenski-Filharmonik-1oz-2022.html", "coin"},
	{"Kangur 2022 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-178-monety-zlote-Australijski-Kangur-1oz-2022-24h.html", "coin"},
	{"Kangur 2022 20d", "31.1", "https://mennicakapitalowa.pl/product-pol-144-moneta-zlota-Australijski-Kangur-1oz-2022-najtaniej.html", "coin"},
	{"Kangur 2022 10d", "31.1", "https://mennicakapitalowa.pl/product-pol-43-moneta-zlota-Australijski-Kangur-2022-1oz.html", "coin"},
	{"Britannia 2022 20d", "31.1", "https://mennicakapitalowa.pl/product-pol-126-moneta-zlota-Britannia-1oz-2022-najtaniej.html", "coin"},
	{"Britannia 2022 10d", "31.1", "https://mennicakapitalowa.pl/product-pol-15-moneta-zlota-Britannia-1oz-2022.html", "coin"},
	{"Liść Klonu 2022 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-227-monety-zlote-Kanadyjski-Lisc-Klonu-1oz-2022-24h.html", "coin"},
	{"Liść Klonu 2022 20d", "31.1", "https://mennicakapitalowa.pl/product-pol-153-moneta-zlota-Kanadyjski-Lisc-Klonu-1oz-2022-najtaniej.html", "coin"},
	{"Liść Klonu 2022 10d", "31.1", "https://mennicakapitalowa.pl/product-pol-60-moneta-zlota-Kanadyjski-Lisc-Klonu-1oz-2022.html", "coin"},
	{"Sztabka 24h", "20", "https://mennicakapitalowa.pl/product-pol-419-Sztabki-zlota-CertiCard-20g-24h.html", "bar"},
	{"Sztabka 10d", "20", "https://mennicakapitalowa.pl/product-pol-36-Sztabki-zlota-CertiCard-20g.html", "bar"},
	{"Sztabka 24h", "31.1", "https://mennicakapitalowa.pl/product-pol-201-Sztabki-zlota-CertiCard-1-uncja-24h.html", "bar"},
	{"Sztabka 10d", "31.1", "https://mennicakapitalowa.pl/product-pol-37-Sztabki-zlota-CertiCard-1-uncja.html", "bar"},
	{"Sztabka 20d", "31.1", "https://mennicakapitalowa.pl/product-pol-140-Sztabki-zlota-CertiCard-1-uncja-najtaniej.html", "bar"},
}

func (s Scraper) Scrape() []*Entry {
	result := []*Entry{}
	priceRegexp := regexp.MustCompile(`"price_net":"([\d.]+)"`)
	for _, template := range urlsToScrape {
		fmt.Println("Fetching", template.URL)
		resp, err := s.client.Get(template.URL)
		entry := &Entry{
			Name:        template.Name,
			Price:       0.0,
			WeightGrams: template.WeightGrams,
			Type:        template.Type,
		}
		result = append(result, entry)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		defer resp.Body.Close()
		rawBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		matches := priceRegexp.FindStringSubmatch(string(rawBody))
		if len(matches) != 2 {
			fmt.Println("Invalid matches:", len(matches))
			continue
		}
		price, err := strconv.ParseFloat(matches[1], 32)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		entry.Price = price
		fmt.Println(entry.Name, entry.Price)
	}
	return result
}
